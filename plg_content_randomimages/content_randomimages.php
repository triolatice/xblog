<?php
/**
 * @package     joomla 2.5 plugin
 * @subpackage  plugins.content_randomimages
 * @version 1.0
 * @since   2.5 
 * @copyright   Copyright (C) 2013 TrioLatice.com. All rights reserved.
 * @license     GNU General Public License version 2 or later when included with or used in the Joomla CMS.
 */

// No direct access
defined('_JEXEC') or die;

// Import library dependencies
JLoader::import('joomla.filesystem.folder');
JLoader::import('joomla.plugin.plugin');
JLoader::import('joomla.filesystem.file');


class plgContentContent_randomimages extends JPlugin
{
	
	
	/**
	 * Example prepare content method
	 *
	 * Method is called by the view
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	object	The content object.  Note $article->text is also available
	 * @param	object	The content params
	 * @param	int		The 'page' number
	 * @since	2.5
	 */
	 
	
	public function onContentPrepare($context, &$article, &$params, $page = 0)
	
    {
			
		// Only display on full article view or blog
    	$view = JRequest::getVar('view','article');
    	$layout = JRequest::getVar('layout','blog');
    	if($view == "article"){
    	} elseif ($layout == "blog") {
    	} else {
    		return;
    	}
        
		//	Add CSS		
		
		//$document->addStyleSheet( JURI::base() . 'plugins/content/content_randomimage/content_randomimage.css' );

		
		
		//	Get Parameters
		$categoryList = $this->params->get('categories');
		$match = false;
		if(empty($categoryList)) {
			$match = true;
		} else {
			for($i=0; $i < count($categoryList); $i++) {
				if(($article->catid) == intval($categoryList[$i])) {
				$match = true;
				}
			}
		
		}
 		if($match==false) {
 			return true;
 		}
		
		
		
		// ispis parametara
		
		$BeforeOrAfterParameter = $this->params->get('BeforeOrAfterParameter','before');
		$AlignmentParameter = $this->params->get('AlignmentParameter','right');
		$imageFolder = $this->params->get('ImageFolder','images');
		$altText = $this->params->get('AltText','images');

		// Initialize Variables
		$imageFiles = '';
		$randomNumber = 0;

		// Retrieve all images in folder
		$imageFiles = JFolder::files( $imageFolder, '(jpg|jpeg|png|gif|xcf|odg|bmp)$', false, false );
		// Select a random value for the image
		$randomNumber = mt_rand(0,count($imageFiles)-1);

		// Prepare the image name and path	
		$filepath = JURI::base() . $imageFolder . "/" . $imageFiles[$randomNumber];

		// Prepare the xHTML
		$imageOutput = '';
		$imageOutput .= "<div class='randomimage' style='float:" . $AlignmentParameter . "'  >";
		$imageOutput .= '<img src="' . $filepath . '" alt="'. $altText . '" /></div>';
		
		// Add the xHTML to the body of the page
		if ($BeforeOrAfterParameter == "before") {
			$article->text = $imageOutput . $article->text;
						
		} else {
			$article->text .= $imageOutput;
				}	
		return true;
	}
	
	
	/**
	 * Example after delete method.
	 *
	 * @param	string	The context for the content passed to the plugin.
	 * @param	object	The data relating to the content that was deleted.
	 * @return	boolean
	 * @since	2.5
	 */
	public function onContentAfterDelete($context, $data)
	{
		return true;
	}

	/**
	 * Example after display content method
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param	string		The context for the content passed to the plugin.
	 * @param	object		The content object.  Note $article->text is also available
	 * @param	object		The content params
	 * @param	int			The 'page' number
	 * @return	string
	 * @since	2.5
	 */
	public function onContentAfterDisplay($context, &$article, &$params, $limitstart)
	{
		$app = JFactory::getApplication();

		return '';
	}

	/**
	 * Example after save content method
	 * Article is passed by reference, but after the save, so no changes will be saved.
	 * Method is called right after the content is saved
	 *
	 * @param	string		The context of the content passed to the plugin (added in 2.5)
	 * @param	object		A JTableContent object
	 * @param	bool		If the content is just about to be created
	 * @since	2.5
	 */
	public function onContentAfterSave($context, &$article, $isNew)
	{
		$app = JFactory::getApplication();

		return true;
	}

	/**
	 * Example after display title method
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param	string		The context for the content passed to the plugin.
	 * @param	object		The content object.  Note $article->text is also available
	 * @param	object		The content params
	 * @param	int			The 'page' number
	 * @return	string
	 * @since	2.5
	 */
	public function onContentAfterTitle($context, &$article, &$params, $limitstart)
	{
		$app = JFactory::getApplication();

		return '';
	}

	/**
	 * Example before delete method.
	 *
	 * @param	string	The context for the content passed to the plugin.
	 * @param	object	The data relating to the content that is to be deleted.
	 * @return	boolean
	 * @since	2.5
	 */
	public function onContentBeforeDelete($context, $data)
	{
		return true;
	}

	/**
	 * Example before display content method
	 *
	 * Method is called by the view and the results are imploded and displayed in a placeholder
	 *
	 * @param	string		The context for the content passed to the plugin.
	 * @param	object		The content object.  Note $article->text is also available
	 * @param	object		The content params
	 * @param	int			The 'page' number
	 * @return	string
	 * @since	2.5
	 */
	public function onContentBeforeDisplay($context, &$article, &$params, $limitstart)
	{
		$app = JFactory::getApplication();

		return '';
	}

	/**
	 * Example before save content method
	 *
	 * Method is called right before content is saved into the database.
	 * Article object is passed by reference, so any changes will be saved!
	 * NOTE:  Returning false will abort the save with an error.
	 *You can set the error by calling $article->setError($message)
	 *
	 * @param	string		The context of the content passed to the plugin.
	 * @param	object		A JTableContent object
	 * @param	bool		If the content is just about to be created
	 * @return	bool		If false, abort the save
	 * @since	2.5
	 */
	public function onContentBeforeSave($context, &$article, $isNew)
	{
		$app = JFactory::getApplication();

		return true;
	}

	/**
	 * Example after delete method.
	 *
	 * @param	string	The context for the content passed to the plugin.
	 * @param	array	A list of primary key ids of the content that has changed state.
	 * @param	int		The value of the state that the content has been changed to.
	 * @return	boolean
	 * @since	2.5
	 */
	public function onContentChangeState($context, $pks, $value)
	{
		return true;
	}

	    
}