<?php
/**
 * @version SVN: $Id: mod_#module#.php 122 2012-10-04 15:07:57Z michel $
 * @package    Yllixadds
 * @subpackage Base
 * @author     Srecko Nagy
 * @license    http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

defined('_JEXEC') or die('Restricted access'); // no direct access

require_once __DIR__ . '/helper.php';
$item = modYllixaddsHelper::getItem($params);
require(JModuleHelper::getLayoutPath('mod_yllixadds'));
require_once ('helper.php');

?>