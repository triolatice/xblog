<?php
/**
 * @package     fsp_menagesr
 * @subpackage  plugins.fsk_editor
 * @copyright   Copyright (C) 2013 TrioLatice.com. All rights reserved.
 * @license     GNU General Public License version 2 or later when included with or used in the Joomla CMS.
 */
/*
 * korištena literatura 
 * Plugin/Events
 * http://http://docs.joomla.org/Plugin/Events
 * http://forum.joomla.org/viewtopic.php?f=628&t=711470
 * http://docs.joomla.org/Supporting_plugins_in_your_component
*/
  
// No direct access.
defined('_JEXEC') or die;
JLoader::import('joomla.plugin.plugin');

class PlgButtonFsp_editor extends JPlugin
{
/**
	 * Constructor
	 *
	 * @access      protected
	 * @param       object  $subject The object to observe
	 * @param       array   $config  An array that holds the plugin configuration
	 * @since       1.0
	 */
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}

	/**
	 * Display the button
	 *
	 * @return array A two element array of (imageName, textToInsert)
	 */
	public function onDisplay($name)
	{
		$app = JFactory::getApplication();

		$doc = JFactory::getDocument();
		// neznam dali je ovo potrebno
		$template = $app->getTemplate();
		// ovo se jos ne koristi  
		// ako se mora provjeriti dali npr dali je već korišten ovakav fsp_editor u tekstu do sada.
		$getContent = $this->_subject->getContent($name);
		$present = JText::_('FSP editor se već korisi u ovome tekstu !', true) ;
		//jInsertEditorText('{test '+txt+'}', editor);  
		
		$js = "
			function insertFsp_editor(editor) {
				var content = $getContent
				if (content.match(/fspsubs/i)) { 
					alert('$present');
					return false;
				} else {
				    txt = prompt('Please enter User Group Id (2 - Registred, 8 - SuperUser )','2,8');
                    if(!txt) return;
					jInsertEditorText('{fspsubs ' +txt+ ' }Your restricted content here{/fspsubs}', editor);
				}
			}
			";

		$doc->addScriptDeclaration($js);
		
		$doc->addStyleSheet( JURI::root().'plugins/editors-xtd/fsp_editor/fsp_editor.css', 'text/css', null, array() );
		//$link = 'index.php?option=com_content&amp;view=article&amp;layout=pagebreak&amp;tmpl=component&amp;e_name='.$name;
		//$link = '../plugins/editors-xtd/my_plg_repertory/plg_window.php';
		JHtml::_('behavior.modal');

		$button = new JObject;
		$button->set('modal', false);
		$button->set('onclick', 'insertFsp_editor(\''.$name.'\');return false;');
		//$button->set('modal', true);
		//$button->set('link', $link);
		$button->set('link', '#');
		$button->set('text', JText::_('FSP editor'));
		$button->set('name', 'fsp_editor');
		$button->set('options', "{handler: 'iframe', size: {x: 400, y: 100}}");

		return $button;
	}	
} //end class
