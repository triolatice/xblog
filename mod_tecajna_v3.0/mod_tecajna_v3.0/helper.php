<?php
# id:  helper.php 14:26 3.1.2009
# package: HNB Tečajna lista
# author: Erik Roznbeker, www.a-web.hr
# Modifkacija za j3.0  : Srečko Nagy  URL : http://www.triolatice.com 
# copyright: (C) 2008-2009 a-web.hr. All rights reserved.
# license: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL, see LICENSE.php
# Notice: Tečajna lista je modificirana za verziju joomla 3.0


defined( '_JEXEC' ) or die( 'Restricted access' );


class modTecajnaHelper{
	
	var $cache_file;
	var $tecajna;
	
	function modTecajnaHelper($cachefile){
		$this->cache_file=$cachefile;
		if(!file_exists($this->cache_file)){ //ako ne postoji - napravi ga
			$fh = fopen($this->cache_file, 'w');
			fclose($fh);
		}
	}
	
	function procitajcache(){
		$cachedata = file_get_contents($this->cache_file);
		
		if(strlen(trim($cachedata))<500 || substr($cachedata, 0, 6)!=date("dmy")){ // prazna ili stari datum
			for($it=0;$it<9;$it++){
				$jucer=(86400*$it);
				$cachedata=$this->dohvaticache(date("dmy",(time()-$jucer)));
				if(strlen(trim(strip_tags($cachedata)))>500 && stristr($cachedata, '<html>') === FALSE){// nije HNB 404
					$this->spremicache($cachedata);
					break;
				}
			}
		}
		return $cachedata;
	}
	
	function dohvaticache($date){
	   $url="http://www.hnb.hr/tecajn/f".$date.".dat";
		if(function_exists("curl_init") && function_exists("curl_setopt") && function_exists("curl_exec") && function_exists("curl_close")){
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			$dat = curl_exec ($ch);
			curl_close ($ch);
			return $dat;
		}
		else if(function_exists("file_get_contents")){
			$dat=@file_get_contents($url);
			return $dat;
		}
		else if(function_exists("file")){
			$dat=@file($url);
			$dat=@implode("",$dat);
			return $dat;
		}
		else if(function_exists("fopen")){
			$fh = @fopen($url, 'r');
			$dat = @fread($fh, 1000);
			@fclose($fh);
			return $dat;
		}
		else echo JText::_('T_NEFUNK');
	}
	
	function spremicache($cachedata){
		$fh = fopen($this->cache_file, 'w');
		fwrite($fh, date("dmy").$cachedata);
		fclose($fh);
	}
	
	function tecajna($odabrane_valute){
		$valute=$this->toArr($odabrane_valute);
		$cachedata=$this->procitajcache();// procitaj cache
		$da=explode("\n",$cachedata);
		if(isset($da[0]))unset($da[0]);
		$tecajna=array();
		$i=0;
		foreach($da as $key=>$d){
			$temp=explode(" ",$d);	
			$temp=array_filter($temp, 'trim');
			if(is_array($temp) && count($temp)){
				$valkey=substr($temp[0], 3, 3);
				if($valkey && in_array($valkey,$valute)){
					$tecajna[$valkey]=array(
						0=>(int)substr($temp[0], 6, 3), //jedinica
						1=>trim($temp[7]), //kupovni
						2=>trim($temp[14]), //srednji
						3=>trim($temp[21]), //prodajni
						4=>$i //redni broj
					);
					$i++;
				}
			}
		}
		$this->tecajna=$tecajna;
		return $tecajna;
	}
	
	function toArr($values){
		$values=explode(' ', $values);
		$values=array_filter($values);
		return $values;
   }
   
   function jsarrays(){
		foreach($this->tecajna as $key=>$tec){
			$kupovni[$key]=$tec[1];
			$srednji[$key]=$tec[2];
			$prodajni[$key]=$tec[3];
			$jedinice[$key]=$tec[0];
			$zemlje[$key]=JText::_('TD_'.$key, 1);
			$monete[$key]=JText::_('TM_'.$key, 1);
		}
		return array($kupovni, $srednji, $prodajni, $jedinice, $zemlje, $monete);
   }
   
   function options($vkey=0){
		$option[]=array('value'=>'-1', 'text'=>'HRK');
		if($vkey && $vkey=='HRK') return '-1';
		$i=0;
		$id=0;
		foreach($this->tecajna as $key=>$tec){
			$option[]=array('value'=>$i, 'text'=>$key);
			if($vkey && $vkey==$key) return $i; //return key
			$i++;
		}
		
		return $option;
   }
}