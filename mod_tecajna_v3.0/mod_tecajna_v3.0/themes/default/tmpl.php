<?php
# id:  default/tmpl.php 14:26 3.1.2009
# package: HNB Tečajna lista
# author: Erik Roznbeker, www.a-web.hr
# Modifkacija za j3.0  : Srečko Nagy  URL : http://www.triolatice.com 
# copyright: (C) 2008-2009 a-web.hr. All rights reserved.
# license: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL, see LICENSE.php
# Notice: Tečajna lista je modificirana za verziju joomla 3.0

defined( '_JEXEC' ) or die( 'Restricted access' );

    $document = JFactory::getDocument();
    $document->addScript( 'modules' .DS.'mod_tecajna'.DS.'inc'.DS.'tecajna.js', 'text/javascript');
    $document->addStyleSheet('modules' .DS.'mod_tecajna'.DS.'inc'.DS.'tecajna.css', $type = 'text/css', $media = 'screen,projection');
if($te_params['tooltip']==1) JHTML::_('behavior.tooltip', '.tecajnaTip', array('className'=>'tecajna'));
?>


<?php if($te_params['prikazipodn']==1){ ?> 
	<div class="hnbtecajna">
		<?php echo $te_params['podnadslov'];?>
	</div>
<?php } ?>

<table width="100%" cellpadding="3" cellspacing="0" class="hnbtecajna">
	<tr>
		<td colspan="<?php echo ($te_params['zastave']==1)?'3':'2';?>" id="tecdatum">
			<?php echo date("d.m.Y");?>
		</td>
		<td>
			<div id="tecvrsta">
				<?php echo $nazivcol[$te_params['vrsta']];?>
			</div>
		</td>
	</tr>
<?php

	foreach($tecajna as $key=>$tec){ ?>
		<tr class="trhnbtecajna tecajnaTip" <?php if($te_params['tooltip']==1){?> title="<?php echo $zemlje[$key];?>::<?php echo $monete[$key];?>"<?php } ?>>
			<td><?php echo $key;?></td>
			<?php if($te_params['zastave']==1){?><td style="text-align:center;">
				<img src="<?php echo $img_put.$key;?>.png" alt="<?php echo $key;?>" />
			</td><?php } ?>
			<td><div class="tkoliko"><?php echo $tec[0];?></div></td>
			<td><div class="tiznos" id="tec<?php echo $tec[4];?>"><?php echo $tec[$te_params['vrsta']];?></div></td>
		</tr>
	<?php
	}
?>
</table>

<?php if($te_params['odabir']==1){ ?>
	<div id="promjenitecaj">
		<a href="javascript:promjenitecaj('1');" id="prom1"><?php echo $nazivcol[1];?></a> | 
		<a href="javascript:promjenitecaj('2');" id="prom2"><?php echo $nazivcol[2];?></a> | 
		<a href="javascript:promjenitecaj('3');" id="prom3"><?php echo $nazivcol[3];?></a>
	</div>
<?php } // odabir ?>

<?php if($te_params['kalkulator']==1){ ?>	
	<table width="100%" cellpadding="3" cellspacing="0" class="hnbtecajna">
		<tr>
			<td class="znak">$</td>
			<td><input type="text" name="iznosin" id="iznosin" value="0" onchange="kalkulator();" onkeyup="kalkulator();" /></td>
			<td>
				<?php echo JHTML::_('select.genericlist', $tl->options(), 'tecajin', 'onchange="kalkulator();" onblur="kalkulator();" size="1"', 'value', 'text', $tl->options('EUR')); ?>
			</td>
			<?php if($te_params['popup']==1){ ?>
				<td>
					<a href="javascript:showhide('pin');" id="pin"><img src="<?php echo $dir_put;?>popup.png" border="0" alt="<?php echo JText::_('T_ODABIR_VALUTE');?>" title="<?php echo JText::_('T_ODABIR_VALUTE');?>" /></a>
				</td>
			<?php } ?>
		</tr>
		<tr>
			<td class="znak">=</td>
			<td><input type="text" name="iznosout" id="iznosout" value="0" onchange="kalkulator();" onkeyup="kalkulator();" /></td>
			<td>
				<?php echo JHTML::_('select.genericlist', $tl->options(), 'tecajout', 'onchange="kalkulator();" onblur="kalkulator();" size="1"', 'value', 'text', '') ?>
			</td>
			<?php if($te_params['popup']==1){ ?>
			<td>
				<a href="javascript:showhide('pout');" id="pout"><img src="<?php echo $dir_put;?>popup.png" border="0" alt="<?php echo JText::_('T_ODABIR_VALUTE');?>" title="<?php echo JText::_('T_ODABIR_VALUTE');?>" /></a>
			</td>
			<?php } ?>
		</tr>
	</table>
<?php if($te_params['popup']==1){ ?>
	<div id="tecajna" style= "visibility:hidden; ">
		<a class="topbarx" href="javascript:showhide()" title="<?php echo JText::_('T_ZATVORI')?>">x</a>
	<?php foreach($tl->options() as $key=>$nv){ ?>
		<a class="tecthumb" href="javascript:selectcurr('<?php echo ($nv['value']+1);?>');" onmouseover="prew('<?php echo $nv['value'];?>','<?php echo $nv['text'];?>');"><?php echo $nv['text'];?></a>
	<?php } ?>
		<br style="clear:both;" />
		<div id="drzava"></div>
		<span style="clear:both;"></span>
		<div align="center"><a href="javascript:showhide(0);"><strong><?php echo JText::_('T_ZATVORI');?></strong></a></div>
	</div>
<?php 	} //popup ?>
<?php } //kalkulator ?>