function gkolumna(id){
	if(id=='1'){var tecarr=kupovni; var naziv=nazivkolumne[0];}
	else if(id=='2'){var tecarr=srednji; var naziv=nazivkolumne[1];}
	else if(id=='3'){var tecarr=prodajni; var naziv=nazivkolumne[2];}
	kolumna=id;
	return new Array(tecarr,naziv);
}

function kalkulator(){
	var tecajin=$('tecajin').value;
	var tecajout=$('tecajout').value;
	var iznosin=$('iznosin').value.replace(',','.');
	var iznosout=$('iznosout');
	var ktecarr=gkolumna(kolumna);
	if(tecajin=='-1')tecajin=1; else tecajin=((ktecarr[0][tecajin].replace(',','.'))/jedinice[tecajin]);
	if(tecajout=='-1')tecajout=1; else tecajout=((ktecarr[0][tecajout].replace(',','.'))/jedinice[tecajout]);
	var trez=(Math.round(100*(tecajin*iznosin)/tecajout))/100+''.replace('.',',');
	if(trez=='NaN')trez=greska;
	iznosout.value=trez;
	if (param_popup==1) $('tecajna').setStyle('visibility', 'hidden');
}

function aktivnakol(id){
	for(var a=1;a<4;a++){
		if(id==a){
			$('prom'+a).className='aktivnakol';
		}
		else{
			$('prom'+a).className='';
		}
	}
}

function promjenitecaj(kol){
	var vkolumna=gkolumna(kol);
	for(var i=0; i<count; i++){
		$('tec'+i).innerHTML=vkolumna[0][i];
	}
	$('tecvrsta').innerHTML=vkolumna[1];
	if (param_kalkulator==1) kalkulator();
	aktivnakol(kol);
}

function showhide(id) {
	lista = id;
	var obj=$(id);
	if(obj){
		var curleft = curtop = 0;
		if (obj.offsetParent) {
			curleft = obj.offsetLeft
			curtop = obj.offsetTop
			while (obj = obj.offsetParent) {
				curleft += obj.offsetLeft
				curtop += obj.offsetTop
			}
		}
	}

	popwin = $('tecajna');
	state = (popwin.getStyle('visibility')=='hidden') ? ('visible') : ('hidden');
	if(curtop && curleft){
		popwin.setStyle('top',curtop);
		popwin.setStyle('left', (curleft+popupstrana));
	}
	popwin.style.visibility = state;
}

function prew(id, kod){
	$('drzava').innerHTML='<div class="drzava">'+t_drzava+':</div><img src="'+img_put+kod+'.png" alt="'+drzava[id]+'"> '+drzava[id];
	$('drzava').innerHTML+='<div class="drzava">'+t_moneta+':</div> '+kod+' - '+moneta[id];
}

function selectcurr(id){
	var kojalista=(lista=='pin')?('tecajin'):('tecajout');
	$(kojalista).options[id].selected=true;
	kalkulator();
}

window.addEvent('domready', function() {
	$$('.trhnbtecajna').each(function(tr){
		tr.addEvent('mouseover', function(e){
			tr.addClass('trhnbtecajnaover');
			tr.removeClass('trhnbtecajna');
		});
		tr.addEvent('mouseout', function(e){
			tr.addClass('trhnbtecajna');
			tr.removeClass('trhnbtecajnaover');
		});
	});
});