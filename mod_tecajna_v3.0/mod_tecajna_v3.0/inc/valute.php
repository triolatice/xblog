<?php
# id:  valute.php 15:29 12.8.2009
# package: HNB Tecajna lista
# author: Erik Roznbeker, www.a-web.hr
# copyright: (C) 2008-2009 a-web.hr. All rights reserved.
# license: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL, see LICENSE.php

defined( '_JEXEC' ) or die( 'Restricted access' );

class JElementValute extends JElement{
   function fetchElement($name, $value, &$node, $control_name){
   global $option;
   
   //Joomfish support
   $joomfish=false;
   if($option=='com_joomfish'){
	   if(!defined('TLJOOMFISH')){
			define('TLJOOMFISH', 1);
			$joomfish=true;
	   }
	   else if(!defined('TLJOOMFISH2')){
			define('TLJOOMFISH2', 1);
			$joomfish=true;
	   }
   }
   
	$val=$this->toArr($value);
	$currencies=array("AUD", "CAD", "CZK", "DKK", "HUF", "JPY", "NOK", "SEK", "CHF", "GBP", "USD", "EUR", "PLN");
	ob_start();
	if(!$joomfish){
?>
	<script>
	var imgyes='<?php echo JUri::base().'images/tick.png'; ?>';
	var imgno='<?php echo JUri::base().'images/publish_x.png'; ?>';
	
	function unesi(param){
		if($(param).src==imgyes){ //now is 'yes' so we set to 'no'
			$('<?php echo $control_name.$name;?>').value=$('<?php echo $control_name.$name;?>').value.replace(new RegExp(param+' ', "g" ), '');
			$(param).src=imgno;
		}
		else{ // now is 'no' so we set to 'yes'
			$('<?php echo $control_name.$name;?>').value+=param+' ';
			$(param).src=imgyes;
		}
	}
	
	<?php if(defined('TLJOOMFISH2')){ // listener for Joomfish "copy" function ?>
	var currencies=new Array("<?php echo implode('","', $currencies);?>");
	window.addEvent('domready', function () {
		window.addEvent('mousedown', function(event) {
			setTimeout('for (x in currencies){ var id = $(currencies[x]); if(id){ var ss= $("<?php echo $control_name.$name;?>").value.search(currencies[x]+" "); if( ss!=-1)id.src=imgyes; else id.src=imgno;	} }', 300);
		});
	});
	<?php } ?>
	
</script>
<?php } ?>
<table class="adminlist" id="tlist" cellspacing="1">
	<thead><tr>
		<th><?php echo JText::_("TEC_DRZAVA");?></th>
		<th><?php echo JText::_("TEC_KOD");?></th>
		<th><?php echo JText::_("TEC_UKLJUCEN");?></th>
	</tr></thead>
	<tbody>
<?php foreach($currencies as $key=>$cur){ ?>
	<tr class="row<?php echo ($key%2==0)?0:1;?>">
		<td><?php echo $this->flag($cur);?> <?php echo JText::_("TD_".$cur);?></td>
		<td><?php echo $cur?></td>
		<td><?php echo $this->simg($cur, $val, $joomfish);?></td>
	</tr>
<?php } ?>
</tbody>
</table>
<input type="hidden" name="<?php echo $control_name.'['.$name.']';?>" id="<?php echo $control_name.$name;?>" value="<?php echo $value ?> " />
<?php
		$out = ob_get_contents();
		ob_end_clean(); 
		return $out;
   }
   
   function simg($cur, $val, $joomfish){
		$img=(in_array($cur, $val))?JUri::base().'images/tick.png':JUri::base().'images/publish_x.png';
		if($joomfish) return '<img src="'.$img.'" alt="'.$cur.'" />';
		else return '<a href="javascript:unesi(\''.$cur.'\');"><img src="'.$img.'" id="'.$cur.'" alt="'.$cur.'" /></a>';
   }
   
   function flag($cur){
		return '<img src="../modules/mod_tecajna/inc/zastave/'.$cur.'.png" alt="'.JText::_("TD_".$cur).'" title="'.JText::_("TD_".$cur).'">';
   }
   
   function toArr($values){
		$values=explode(' ', $values);
		$values=array_filter($values);
		return $values;
   }
}