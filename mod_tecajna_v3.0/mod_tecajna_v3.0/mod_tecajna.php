<?php
# id:  mod_tecajna.php 14:26 3.1.2009
# package: HNB Tečajna lista
# author: Erik Roznbeker, www.a-web.hr
# copyright: (C) 2008-2009 a-web.hr. All rights reserved.
# license: http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL, see LICENSE.php

defined( '_JEXEC' ) or die( 'Restricted access' );
if(!defined('DS')){
    define('DS',DIRECTORY_SEPARATOR);
}



//cache file
$cache_dat=dirname(__FILE__)."/inc/cache.dat";

//paths
$dir_put="modules/mod_tecajna/inc/";
$img_put=$dir_put."zastave/";

//helper class
require_once __DIR__ . '/helper.php';
$tl=new modTecajnaHelper($cache_dat);

//parameters
$te_params['valute'] = (strlen(trim($params->get( 'valute' )))==0)?("EUR,USD,GBP"):($params->get( 'valute' ));
$te_params['zastave'] = (strlen(trim($params->get( 'zastave' )))==0)?(1):($params->get( 'zastave' ));
$te_params['vrsta'] = (strlen(trim($params->get( 'vrsta' )))==0)?(2):($params->get( 'vrsta' ));
$te_params['moduleclass_sfx'] = (strlen(trim($params->get( 'moduleclass_sfx' )))==0)?(NULL):($params->get( 'moduleclass_sfx' ));
$te_params['css'] = (strlen(trim($params->get( 'css' )))==0)?(1):($params->get( 'css' ));
$te_params['podnadslov'] = $params->get( 'podnadslov' );
$te_params['prikazipodn'] = (strlen($params->get( 'prikazipodn' ))==0)?(1):($params->get( 'prikazipodn' ));
$te_params['odabir'] = (strlen($params->get( 'odabir' ))==0)?(1):($params->get( 'odabir' ));
$te_params['kalkulator'] = (strlen($params->get( 'kalkulator' ))==0)?(1):($params->get( 'kalkulator' ));
$te_params['popup'] = (strlen($params->get( 'popup' ))==0)?(1):($params->get( 'popup' ));
$te_params['popupstrana'] = (strlen($params->get( 'popupstrana' ))==0)?("D"):($params->get( 'popupstrana' ));
$te_params['tooltip'] = (strlen($params->get( 'tooltip' ))==0)?(1):($params->get( 'tooltip' ));

//vars
$nazivcol=array(1=>JText::_('T_KUPOVNI'),2=>JText::_('T_SREDNJI'),3=>JText::_('T_PRODAJNI'));

$tecajna=$tl->tecajna($te_params['valute']);// tecajna arr

list($kupovni, $srednji, $prodajni, $jedinice, $zemlje, $monete)=$tl->jsarrays();
echo '<!-- Modul HNB tecajna lista, www.a-web.hr-->';
if(is_array($tecajna) && count($tecajna)){
?>
<script language="javascript" type="text/javascript">
	var kupovni=new Array('<?php echo implode("','",$kupovni);?>');
	var srednji=new Array('<?php echo implode("','",$srednji);?>');
	var prodajni=new Array('<?php echo implode("','",$prodajni);?>');
	var jedinice=new Array('<?php echo implode("','",$jedinice);?>');//Thank you Daniel
	var kolumna="<?php echo $te_params['vrsta'];?>";
	var nazivkolumne=new Array('<?php echo implode("','",$nazivcol);?>');
	var greska ='<?php echo JText::_('T_GRESKA', 1);?>';
	var param_popup=<?php echo ($te_params['popup']==1)?'1':'0';?>;
	var param_kalkulator=<?php echo ($te_params['kalkulator']==1)?'1':'0';?>;
	var count = <?php echo count($kupovni);?>;
	var popupstrana = <?php echo($te_params['popupstrana']=="L")?"-205":"15"; // popup lijevo ili desno ?>;
	var lista;
	var drzava=new Array('<?php echo implode("','",$zemlje);?>');
	var moneta=new Array('<?php echo implode("','",$monete);?>');
	drzava['-1']='<?php echo JText::_('TD_HRK', 1);?>';
	moneta['-1']='<?php echo JText::_('TM_HRK', 1);?>';
	var t_drzava='<?php echo JText::_('T_DRZAVA', 1);?>';
	var img_put='<?php echo $img_put;?>';
	var t_moneta='<?php echo JText::_('T_MONETA', 1);?>';
	window.onload=function(){
		<?php if($te_params['odabir']==1){ ?>aktivnakol(kolumna);<?php } ?>
		prew('-1','HRK');
	}
</script>
<?php
	//template
	require_once (__DIR__ .DS.'themes'.DS.'default'.DS.'tmpl.php');
	
}  
else echo JText::_('T_OFFLINEGRESKA');
?>